<?php
//All implemented site actions config
    return [
        //user actions
        'user/login' => 'user/login',
        'user/logout' => 'user/logout',
        'user/register' => 'user/register',
        //product actions
        'product/list' => 'product/list',
        'product/create' => 'product/create',
        'product/delete' => 'product/delete',
        'product/edit' => 'product/edit',
        //category actions
        'category/list' => 'category/list',
        'category/create' => 'category/create',
        'category/delete' => 'category/delete',
        'category/edit' => 'category/edit',
        'category/products' => 'category/categoryProducts',
        'category/add-product' => 'category/addProduct',
        'category/delete-product' => 'category/deleteProduct',
        //testing
        'test/user/login' => 'test/UserLogin',
        'test/user/register' => 'test/UserRegister',
        'test/product/create' => 'test/ProductCreate',
        'test/product/delete' => 'test/ProductDelete',
        'test/product/edit' => 'test/ProductEdit',
        'test/category/create' => 'test/CategoryCreate',
        'test/category/delete' => 'test/CategoryDelete',
        'test/category/edit' => 'test/CategoryEdit',
        'test/category/products' => 'test/CategoryProducts',
        'test/category/add-product' => 'test/CategoryAddProduct',
        'test/category/delete-product' => 'test/CategoryDeleteProduct',
        
        '' => 'site/index',
    ];