<?php
//User actions permissions config
    return [
        'ProductController/actionCreate' => true,
        'ProductController/actionDelete' => true,
        'ProductController/actionEdit' => true,
        
        'CategoryController/actionCreate' => true,
        'CategoryController/actionDelete' => true,
        'CategoryController/actionEdit' => true,
        'CategoryController/actionAddProduct' => true,
        'CategoryController/actionDeleteProduct' => true,
    ];