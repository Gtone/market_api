<?php
/**
 * Product model
 *
 * @author User
 */
class Product {
    /**
     * Create new product record in DB
     * @param string $productName
     * @return array
     */
    public static function createProduct($productName){
        $db = Db::getConnection();
        $sql = "INSERT INTO product (name) "
                . "VALUES (:name);";
        $result = $db->prepare($sql);
        $result->bindParam(':name', $productName, PDO::PARAM_STR);
        return $result->execute();
    }
    
    /**
     * Checks for product record existense
     * @param string $productName
     * @return NULL||array
     */
    public static function getProductByName($productName) {
        $db = Db::getConnection();
        $sql = "SELECT * FROM product WHERE name = :name;";
        $result = $db->prepare($sql);
        $result->bindParam(':name', $productName, PDO::PARAM_STR);
        $result->execute();
        //Return null if record empty
        if ($result === false) {
            return null;
        }
        //Fetch db record if exec success
        $row = $result->fetch(PDO::FETCH_ASSOC);
        
        return $row;
    }
    
    /**
     * Select product by id from db 
     * @param int $id
     * @return NULL||array
     */
    public static function getProductById($id) {
        $id = intval($id);
        
        if($id) {
            $db = Db::getConnection();
            $sql = "SELECT * FROM product WHERE id = :id;";
            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);
            $result->execute();
            //Return null if record empty
            if ($result === false) {
                return null;
            }
            //Fetch db record if exec success
            $row = $result->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
    }
    
    /**
     * Select all products records from db
     * @return array
     */
    public static function getProductsList() {
        $db = Db::getConnection();
        
        $result = $db->query("SELECT * FROM product");

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
     * Delete product from db
     * @param int $id
     */
    public static function deleteProductById($id) {
        $db = Db::getConnection();
        
        $sql = 'DELETE FROM product WHERE id = :id;';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        //delete data from parent table
        $sql = 'DELETE FROM products_in_categories WHERE product_id = :id;';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }
    
    /**
     * Check product existence
     * @param int $id
     * @return boolean
     */
    public static function checkProductId ($id) {
        $db = Db::getConnection();
        $sql = ("SELECT * FROM product WHERE id = :id;");
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $count = $result->rowCount();
        //Checking for 0 rows,
        // when rows in record is 0, there is no this kind of record
        if ($count === 0) {
            return false;
        } else {
            return true;
        }
    }
    /**
     * Update data in product record
     * @param int $id
     * @param str $name
     * @return array
     */
    public static function updateProduct($id, $name) {
        $db = Db::getConnection();
        $sql = "UPDATE product SET name = :name WHERE id=:id;";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        return $result->execute();
    }
}
