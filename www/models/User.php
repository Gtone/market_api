<?php

/**
 * Description of User
 *
 * @author User
 */
class User {
    public static function createUser($login, $email, $password) {
        $db = Db::getConnection();
        
        $sql = 'INSERT INTO user (login, email, password)'
                . 'VALUES (:login, :email, :password);';
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function checkLogged () {
        if(isset($_SESSION['user_id'])){
            return $_SESSION['user_id'];
        }
        //Error if not logged in
        throw new Exception("You must be authorized");
    }
    /**
     * Select admin record from database, by login from param
     * @param type $login
     * @return database record (fetch result) or null
     */
    public static function getUserByLogin ($login) {
        $db = Db::getConnection();
        
        $sql = "SELECT * FROM user WHERE login = (:login);";
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->execute();
        //Return null if record empty
        if ($result === false) {
            return null;
        }
        //Fetch db record if exec success
        $row = $result->fetch(PDO::FETCH_ASSOC);
        
        return $row;
    }
}
