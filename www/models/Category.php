<?php
/**
 * Category model
 *
 * @author User
 */
class Category {
    /**
     * Select category from DB
     * @param int $id
     * @return array
     */
    public static function getCategoryById($id) {
        $id = intval($id);
        if($id) {
            $db = Db::getConnection();
            $sql = "SELECT * FROM category WHERE id = :id;";
            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);
            $result->execute();
            //Return null if record empty
            if ($result === false) {
                return null;
            }
            //Fetch db record if exec success
            $row = $result->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
    }
    
    /**
     * Return all categories records from db
     * @return array
     */
    public static function getCategoriesList() {
        $db = Db::getConnection();
        
        $result = $db->query("SELECT * FROM category");

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public static function getCategoryByName($categoryName) {
        $db = Db::getConnection();
        $sql = "SELECT * FROM category WHERE name = :name;";
        $result = $db->prepare($sql);
        $result->bindParam(':name', $categoryName, PDO::PARAM_STR);
        $result->execute();
        //Return null if record empty
        if ($result === false) {
            return null;
        }
        //Fetch db record if exec success
        $row = $result->fetch(PDO::FETCH_ASSOC);
        
        return $row;
    }
    
    /**
     * Create new category record in DB
     * @param string $categoryName
     * @return array
     */
    public static function createCategory($categoryName){
        $db = Db::getConnection();
        $sql = "INSERT INTO category (name) "
                . "VALUES (:name);";
        $result = $db->prepare($sql);
        $result->bindParam(':name', $categoryName, PDO::PARAM_STR);
        return $result->execute();
    }
    
    /**
     * Delete data from certain category
     * @param int $id
     * @return array
     */
    public static function deleteCategoryById($id) {
        $db = Db::getConnection();

        $sql = 'DELETE FROM category WHERE id = :id;';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        //delete data from parent table
        $sql = 'DELETE FROM products_in_categories WHERE category_id = :id;';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }
    /**
     * Update data in category record
     * @param int $id
     * @param str $name
     * @return array
     */
    public static function updateCategory($id, $name) {
        $db = Db::getConnection();
        $sql = "UPDATE category SET name = :name WHERE id=:id;";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        return $result->execute();
    }
    
    public static function getProductsInCategory($id) {
        $db = Db::getConnection();
        $sql = "SELECT p.id, p.name FROM product p
                JOIN products_in_categories pc ON p.id = pc.product_id
                WHERE pc.category_id = :id;";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
     * Apply product to category
     * @param int $id
     * @param int $productId
     * @return array
     */
    public static function addProductInCat($id, $productId) {
        $db = Db::getConnection();
        $sql = "INSERT INTO products_in_categories (category_id, product_id)"
                . "VALUES(:id, :product_id);";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':product_id', $productId, PDO::PARAM_STR);
        return $result->execute();
    }
    
    /**
     * Deletes profuct from certain category
     * @param int $category_id point to category
     * @param int $product_id 
     */
    public static function deleteProductFromCat($category_id, $product_id) {
        $db = Db::getConnection();

        $sql = 'DELETE FROM products_in_categories '
                . 'WHERE category_id = :category_id AND product_id = :product_id;';
        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $category_id, PDO::PARAM_INT);
        $result->bindParam(':product_id', $product_id, PDO::PARAM_INT);
        $result->execute();
    }
    
    /**
     * Pics one record from products in categories table, with concrete params
     * @param int $id
     * @param int $product_id
     * @return bool
     */
    public static function checkProductInCategory($category_id, $product_id) {
        $db = Db::getConnection();
        $sql = "SELECT * FROM products_in_categories "
                . "WHERE category_id = :category_id AND product_id = :product_id;";
        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $category_id, PDO::PARAM_INT);
        $result->bindParam(':product_id', $product_id, PDO::PARAM_STR);
        $result->execute();
        $count = $result->rowCount();
        //Checking for 0 rows,
        // when rows in record is 0, there is no this kind of record
        if ($count === 0) {
            return false;
        } else {
            return true;
        }
    }
}
