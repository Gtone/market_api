<?php
/**
 * Description of TestController
 *
 * @author User
 */
class TestController {
    //User test
    public function actionUserLogin() {
        echo "<form method='post' action='/user/login/'>";
            echo "<label>Login:</label>";
            echo "<input type='text' name='login'>";
            echo "<label>Password:</label>";
            echo "<input type='text' name='password'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    public function actionUserRegister() {
        echo "<form method='post' action='/user/register/'>";
            echo "<label>Login:</label>";
            echo "<input type='text' name='login'>";
            echo "<label>Email:</label>";
            echo "<input type='email' name='email'>";
            echo "<label>Password:</label>";
            echo "<input type='text' name='password'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    
    //Product test
    public function actionProductCreate() {
        echo "<form method='post' action='/product/create/'>";
            echo "<label>Product name:</label>";
            echo "<input type='text' name='product_name'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    public function actionProductDelete() {
        echo "<form method='post' action='/product/delete/'>";
            echo "<label>Product id:</label>";
            echo "<input type='text' name='product_id'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    public function actionProductEdit() {
        echo "<form method='post' action='/product/edit/'>";
            echo "<label>Product id:</label>";
            echo "<input type='text' name='product_id'>";
            echo "<label>Product name:</label>";
            echo "<input type='text' name='product_name'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    
    //Category test
    public function actionCategoryCreate() {
        echo "<form method='post' action='/category/create/'>";
            echo "<label>Category name:</label>";
            echo "<input type='text' name='category_name'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    public function actionCategoryDelete() {
        echo "<form method='post' action='/category/delete/'>";
            echo "<label>Category id:</label>";
            echo "<input type='text' name='category_id'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    public function actionCategoryEdit() {
        echo "<form method='post' action='/category/edit/'>";
            echo "<label>Category id:</label>";
            echo "<input type='text' name='category_id'>";
            echo "<label>Category name:</label>";
            echo "<input type='text' name='category_name'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    public function actionCategoryProducts() {
        echo "<form method='post' action='/category/products/'>";
            echo "<label>Category id:</label>";
            echo "<input type='text' name='category_id'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    public function actionCategoryAddProduct() {
        echo "<form method='post' action='/category/add-product/'>";
            echo "<label>Category id:</label>";
            echo "<input type='text' name='category_id'>";
            echo "<label>Product id:</label>";
            echo "<input type='text' name='product_id'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    public function actionCategoryDeleteProduct() {
        echo "<form method='post' action='/category/delete-product/'>";
            echo "<label>Category id:</label>";
            echo "<input type='text' name='category_id'>";
            echo "<label>Product id:</label>";
            echo "<input type='text' name='product_id'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
}
