<?php
/**
 * Default controller, for wrong actions no routes config actions
 */
class DefaultController {
    /**
     * Redirects to 404 template page
     * @return boolean
     */
    public function default404() {
        require_once ('views/layouts/404.php');
        
        return true;
    }
}
