<?php
/**
 * Controller for user actions
 *
 * @author User
 */
class UserController {
    /**
     * Set to $_SESSION user_id from post,
     * checks user exitense from database record
     * @throws Exception
     */
    public function actionLogin() {
        if (isset($_SESSION["user_id"])) {
            throw new Exception("Already logged");
        }
        //Get params from post
        if(!key_exists("login", $_POST) || !$_POST["login"]) {
            throw new Exception("Incorrect login");
        }
        $login = $_POST["login"];
        if(!key_exists("password", $_POST) || !$_POST["password"]) {
            throw new Exception("Incorrect password");
        }
        $password = $_POST["password"];
        
        //User login
        $user = User::getUserByLogin($login);
        $password = md5($password);
        if($user["password"] != $password) {
            throw new Exception("Password doesn't match to user");
        }
        //Insert session after login
        $_SESSION["user_id"] = $user["id"];
        //Return result to client
        $result = ['status' => 1];
        echo json_encode($result);
    }
    
    /**
     * Registration action for user,
     * creates new record for user in database
     * @throws Exception
     */
    public function actionRegister() {
        //Get params from post
        if(!key_exists("login", $_POST) || !$_POST["login"]) {
            throw new Exception("Incorrect login");
        }
        $login = $_POST["login"];
        if(!key_exists("password", $_POST) || !$_POST["password"]) {
            throw new Exception("Incorrect password");
        }
        $password = $_POST["password"];
        if(!key_exists("email", $_POST) || !$_POST["email"] || !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Incorrect email");
        }
        $email = $_POST["email"];
        
        //Insert data into DB
        $user = User::getUserByLogin($login);
        if ($user) {
            throw new Exception("User with this login exists");
        }
        $password = md5($password);
        User::createUser($login, $email, $password);
        $result = ['status' => 1];
        echo json_encode($result);
    }
    
    /**
     * Removes user id from session
     */
    public function actionLogout () {
        if(!key_exists("user_id", $_SESSION) || !$_SESSION["user_id"]) {
            throw new Exception("User not logged");
        }
        unset($_SESSION["user_id"]);
        $result = ['status' => 1];
        echo json_encode($result);
    }
}
