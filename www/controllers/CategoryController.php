<?php
/**
 * Category Controller
 *
 * @author User
 */
class CategoryController {
    /**
     * @return json list of categories
     */
    public function actionList() {
        $result = Category::getCategoriesList();
        
        echo json_encode($result);
    }
    
    /**
     * Creates new category
     * @throws Exception
     */
    public function actionCreate() {
        //Get params from post
        if(!key_exists("category_name", $_POST) || !$_POST["category_name"]) {
            throw new Exception("Incorrect category name");
        }
        $name = $_POST["category_name"];
        
        //Insert data into DB
        $category = Category::getCategoryByName($name);
        if ($category) {
            throw new Exception("Category with this name already exists");
        }
        Category::createCategory($name);
        $result = ['status' => 1];
        echo json_encode($result);
    }
    
    /**
     * Delete category from db by id from POST
     * @throws Exception
     */
    public function actionDelete() {
        //Get params from post
        if(!key_exists("category_id", $_POST) || !$_POST["category_id"]) {
            throw new Exception("Incorrect category id");
        }
        $id = $_POST["category_id"];
        //check id if not - wrong id
        $category = Category::getCategoryById($id);
        if (!$category) {
            throw new Exception("Category id doesn't exists");
        }
        Category::deleteCategoryById($id);
        $result = ['status' => 1];
        echo json_encode($result);
    }
    
    /**
     * Edits category name by id from POST
     * @throws Exception
     */
    public function actionEdit() {
        //Get params from post
        if(!key_exists("category_id", $_POST) || !$_POST["category_id"]) {
            throw new Exception("Incorrect category id");
        }
        $id = $_POST["category_id"];
        if(!key_exists("category_name", $_POST) || !$_POST["category_name"]) {
            throw new Exception("Incorrect category name");
        }
        $name = $_POST["category_name"];
        //check id if not - wrong id
        $category = Category::getCategoryById($id);
        if (!$category) {
            throw new Exception("Category id doesn't exists");
        }
        //Update DB data
        Category::updateCategory($id, $name);
        $result = ['status' => 1];
        echo json_encode($result);
    }
    
    /**
     * Shows products in certain category
     * @throws Exception
     */
    public function actionCategoryProducts () {
        //Get params from post
        if(!key_exists("category_id", $_POST) || !$_POST["category_id"]) {
            throw new Exception("Incorrect category id");
        }
        $id = $_POST["category_id"];
        //check id if not - wrong id
        $category = Category::getCategoryById($id);
        if (!$category) {
            throw new Exception("Category id doesn't exists");
        }
        $result = Category::getProductsInCategory($id);
        echo json_encode($result);
    }
    
    /**
     * Adds product to existing category
     * @throws Exception
     */
    public function actionAddProduct () {
        //Get params from post
        if(!key_exists("category_id", $_POST) || !$_POST["category_id"]) {
            throw new Exception("Incorrect category id");
        }
        $categoryId = $_POST["category_id"];
        if(!key_exists("product_id", $_POST) || !$_POST["product_id"]) {
            throw new Exception("Incorrect product id");
        }
        $productId = $_POST["product_id"];
        //Check for product and category existing
        $category = Category::getCategoryById($categoryId);
        $product = Product::getProductById($productId);
        if (!$category) {
            throw new Exception("Category id doesn't exists");
        }
        if (!$product) {
            throw new Exception("Product id doesn't exists");
        }
        //Check if product already exists for this category
        $productCheck = Category::checkProductInCategory($categoryId, $productId);
        if ($productCheck) {
            throw new Exception("Product already in this category");
        }
        Category::addProductInCat($categoryId, $productId);
        $result = ['status' => 1];
        echo json_encode($result);
    }
    
    /**
     * Deletes product from certain category
     * @throws Exception
     */
    public function actionDeleteProduct () {
        //Get params from post
        if(!key_exists("category_id", $_POST) || !$_POST["category_id"]) {
            throw new Exception("Incorrect category id");
        }
        $categoryId = $_POST["category_id"];
        if(!key_exists("product_id", $_POST) || !$_POST["product_id"]) {
            throw new Exception("Incorrect product id");
        }
        $productId = $_POST["product_id"];
        //Check for product and category existing
        $category = Category::getCategoryById($categoryId);
        $product = Product::getProductById($productId);
        if (!$category) {
            throw new Exception("Category id doesn't exists");
        }
        if (!$product) {
            throw new Exception("Product id doesn't exists");
        }
        Category::deleteProductFromCat($categoryId, $productId);
        $result = ['status' => 1];
        echo json_encode($result);
    }
}
