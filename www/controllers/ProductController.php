<?php
/**
 * Controller class for products
 *
 * @author User
 */
class ProductController {
    /**
     * Get list of products
     */
    public function actionList() {
        $result = Product::getProductsList();
        
        echo json_encode($result);
    }
    
    /**
     * Creates new product
     * @throws Exception
     */
    public function actionCreate() {
        //Get params from post
        if(!key_exists("product_name", $_POST) || !$_POST["product_name"]) {
            throw new Exception("Incorrect product name");
        }
        $name = $_POST["product_name"];
        
        //Insert data into DB
        $product = Product::getProductByName($name);
        if ($product) {
            throw new Exception("Product with this name already exists");
        }
        Product::createProduct($name);
        $result = ['status' => 1];
        echo json_encode($result);
    }
    
    /**
     * Delete product from DB by id from POST
     * @throws Exception
     */
    public function actionDelete() {
        //Get params from post
        if(!key_exists("product_id", $_POST) || !$_POST["product_id"]) {
            throw new Exception("Incorrect product id");
        }
        $id = $_POST["product_id"];
        //check id if not - wrong id
        $product = Product::getProductById($id);
        if (!$product) {
            throw new Exception("Product id doesn't exists");
        }
        //delete data from DB
        Product::deleteProductById($id);
        $result = ['status' => 1];
        echo json_encode($result);
    }
    
    /**
     * Edit product name by id from POST
     * @throws Exception
     */
    public function actionEdit() {
        //Get params from post
        if(!key_exists("product_id", $_POST) || !$_POST["product_id"]) {
            throw new Exception("Incorrect product id");
        }
        $id = $_POST["product_id"];
        if(!key_exists("product_name", $_POST) || !$_POST["product_name"]) {
            throw new Exception("Incorrect product name");
        }
        $name = $_POST["product_name"];
        //check id if not - wrong id
        $product = Product::getProductById($id);
        if (!$product) {
            throw new Exception("Product id doesn't exists");
        }
        //Update DB data
        Product::updateProduct($id, $name);
        $result = ['status' => 1];
        echo json_encode($result);
    }
}
