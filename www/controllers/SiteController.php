<?php
/**
 * Site controller class
 * can be customized for main page actions
 */
class SiteController {
    /**
     * Shows index action
     */
    public function actionIndex(){
        print "Index";
    }
}
