<?php   
//FRONT CONTROLLER
    //Common settings
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    
    session_start();
    
    //System file includes
    define('ROOT', dirname(__FILE__));
    require_once (ROOT . '/components/Autoload.php');
    try { 
        //Exec router
        $router = new Router();
        $router->run();
    } catch (Exception $ex) {
        $error_message = $ex->getMessage();
        $result = [
            'status' => 0,
            'message' => $error_message,
        ];
        echo json_encode($result);
    }